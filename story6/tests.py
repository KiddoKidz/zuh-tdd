from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
import time
from . import views 
from .models import StatusModel
from .apps import Story6Config
from django.urls import resolve
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

# Create your tests here.

class StatusUnitTest(TestCase):
    def test_app_name(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')

    def test_index_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_other_url(self):
        response = Client().get('/wek/')
        self.assertEqual(response.status_code,404)
    
    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_index_template(self):
        response = Client().get('/')
        self.assertContains(response, 'How was your day?', count=1, html=True)

    def test_save_status(self):
        data_test = StatusModel(status="Im fine")
        data_test.save()
        self.assertIsInstance(data_test, StatusModel)
        self.assertEqual(StatusModel.objects.all().count(), 1)

    def test_post_request(self):
        data_post = {'status':'Im having a good day'}
        request = Client().post('/', data_post, follow=True)
        self.assertEqual(StatusModel.objects.count(),1)

        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Im having a good day',count=1, html=True)

    def test_saving_600_chars_status(self):
        chars = "AA AA"*100000
        # print(chars)
        data_test = StatusModel(status=chars)
        data_test.save()
        self.assertEqual(StatusModel.objects.all().count(), 1)

class StatusFunctionalTest(LiveServerTestCase):
    def setUp(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('headless')
        self.options.add_argument('no-sandbox')
        self.options.add_argument('disable-dev-shm-usage')
        self.options.add_argument('disable-extensions')
        self.browser = webdriver.Chrome(options=self.options, executable_path='./chromedriver')
        return super().setUp()
    
    def tearDown(self):
        self.browser.quit()
        return super().tearDown()

    def test_index_page_loaded(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Index', self.browser.title)
        # self.fail('FInisht the test')

    def test_submit_status(self):
        self.browser.get(self.live_server_url)

        wait = WebDriverWait(self.browser, 10)

        element = wait.until(EC.element_to_be_clickable((By.CLASS_NAME,'btn')))
        text_box = self.browser.find_element_by_id('Your_Status')
        button = self.browser.find_element_by_class_name('btn')

        time.sleep(3)

        actions = webdriver.ActionChains(self.browser)
        actions.click(on_element=text_box)
        actions.send_keys("Coba Coba")
        actions.click(button)
        actions.pause(2)
        actions.perform()

        element = wait.until(EC.element_to_be_clickable((By.CLASS_NAME,'btn')))
        text_box = self.browser.find_element_by_id('Your_Status')
        button = self.browser.find_element_by_class_name('btn')

        time.sleep(1)
        
        actions = webdriver.ActionChains(self.browser)
        actions.click(on_element=text_box)
        actions.send_keys("SECOND STATUS")
        actions.click(button)
        actions.pause(2)
        actions.perform()

        list_status_in_page = self.browser.find_elements_by_class_name('status_detail')
        list_status_in_db = StatusModel.objects.all()
        if((len(list_status_in_page) > 0) & (list_status_in_db.count() > 0)):
            for i in range(0,list_status_in_db.count()):
                # print(list_status_in_page[0].text)
                # print(list_status_in_db[0].status)
                self.assertEqual(list_status_in_page[i].text, list_status_in_db[i].status)

        # self.fail("Finish testing")

    def test_600_chars_input(self):
        self.browser.get(self.live_server_url)

        wait = WebDriverWait(self.browser, 10)

        element = wait.until(EC.element_to_be_clickable((By.CLASS_NAME,'btn')))
        text_box = self.browser.find_element_by_id('Your_Status')
        button = self.browser.find_element_by_class_name('btn')

        chars = "A"*600

        actions = webdriver.ActionChains(self.browser)
        actions.click(on_element=text_box)
        actions.send_keys(chars)
        actions.click(button)
        actions.pause(2)
        actions.perform()

        showed_status = self.browser.find_element_by_class_name('status_detail')
        self.assertEqual(showed_status.text, "A"*300)