$(document).ready(() => {
    var icons = {
        header: "ui-icon-caret-2-n-s",
        activeHeader: "ui-icon-caret-2-e-w"
    }
    $("#accordion").accordion({
        icons: icons,
        collapsible : true
    })

    $("input.cb").click(() => {
        console.log("bb")
        if ($("body").hasClass('light')){
            $('body').removeClass("light")
            $('body').addClass("dark")
            // $("body").css({transition:'.5s ease',backgroundColor: "#2b2b28"})
            // $(".switch label").css({color: '#fff'})
            // $(".fp img").css({boxShadow:"0px 0px 25px 3px rgba(112,169,90,1)"})
        } else {
            $('body').removeClass("dark")
            $('body').addClass("light")
            // $("body").css({transition:'.5s ease',backgroundColor: "#c9d99e"})
            // $(".switch label").css({color: '#000'})
            // $(".fp img").css({boxShadow:"0px 0px 25px 3px rgba(92,92,92,1)"})
        }
    })
    $('#searchForm').on('submit', (e)=>{
        let input = $('#search').val()        
        // console.log(input)
        search(input);
        e.preventDefault();
    })
})

function  search(search) {
    $.ajax('https://www.googleapis.com/books/v1/volumes?q='+search)
    .done(response => {
        let bukus = response.items
        // console.log(bukus)
        let output = ``
        $.each(bukus, (index, buku)=>{
            let info = buku.volumeInfo;
            let title = info.title;
            let authors = info.authors;
            if(authors == undefined){
                authors = "-";
            }
            let image = info.imageLinks.thumbnail;
            let pub = info.publisher;
            if(pub == undefined){
                pub = "-"
            }
            // $.each(info.authors, (index, author) => {
            //     if(index == info.authors.length-1){
            //         authors += author;
            //     } else {                    
            //         authors += author + ', ';
            //     }
            // })
            output +=`
                <tr>
                    <td><div><p>${title}</p></div></td>
                    <td><div><p>${authors}</p></div></td>
                    <td><div><p>${pub}</p></div></td>
                    <td><div><img src=${image}></div></td>
                </tr>

            `
            // console.log(buku)
        })
        $('.table-body').html(output)
    })
    .fail()

}
