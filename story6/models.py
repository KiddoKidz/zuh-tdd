from django.db import models

# Create your models here.

class StatusModel(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)