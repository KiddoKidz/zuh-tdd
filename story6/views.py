from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from .forms import StatusForm
from .models import StatusModel

# Create your views here.

def index(request):
    context = {}
    forms = StatusForm()
    if request.method == 'POST':
        status = request.POST['status']
        new_status  = StatusModel()
        new_status.status = status
        new_status.save()
        return redirect(reverse('story6:index'))
    status_list = StatusModel.objects.all()
    context['forms'] = forms
    context['status_list'] = status_list
    # print(context['status_list'].count())
    return render(request, 'index.html', context)