from django.forms import Form
from django import forms

class StatusForm(Form):
    status = forms.CharField(max_length=300,widget=forms.TextInput(attrs={'class':'validate','id':'Your_Status', 'tag':'Your Status'}))