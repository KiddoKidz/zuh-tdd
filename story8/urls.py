from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.book_index, name='book_index')
]
