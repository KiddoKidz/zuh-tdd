from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.apps import apps
import time
from .apps import Story8Config
from . import views 
from django.urls import resolve
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
# Create your tests here.

class Story8UnitTest(TestCase):
    def test_app_name(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')

    def test_book_url(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)
    
    def test_book_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, views.book_index)

    def test_book_template(self):
        response = Client().get('/books/')
        self.assertContains(response, 'Want to find some books?', count=1, html=True)

class Story8FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('headless')
        self.options.add_argument('no-sandbox')
        self.options.add_argument('disable-dev-shm-usage')
        self.options.add_argument('disable-extensions')
        self.browser = webdriver.Chrome(options=self.options, executable_path='./chromedriver')
        return super().setUp()
    
    def tearDown(self):
        self.browser.quit()
        return super().tearDown()
    
    def test_search_book(self):
        self.browser.get(self.live_server_url+'/books/')
        time.sleep(10)

        temp = self.browser.find_element_by_id('search')

        actions = webdriver.ActionChains(self.browser)
        actions.click(temp)
        actions.send_keys("home")
        actions.perform()