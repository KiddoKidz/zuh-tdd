from django.shortcuts import render, HttpResponse

# Create your views here.
def book_index(request):
    return render(request, 'book_index.html')