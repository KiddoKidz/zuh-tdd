from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.apps import apps
import time
from .apps import Story9Config
from . import views as story9_views
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import User
from django.urls import resolve
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class Story9UnitTest(TestCase):
    

    def test_app_name(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')

    
    def test_login_url(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_account(self):
        self.credentials = {
            'username': 'test',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
        response = self.client.post('/user/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)
    
    def test_login_template(self):
        response = Client().get('/user/login/')
        self.assertContains(response, 'Log In', count=1, html=True)
        self.assertTemplateUsed(response, template_name='registration/login.html')

    def test_logout_url(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code, 302)

    def test_signup_url(self):
        response = Client().get('/user/signup/')
        self.assertEqual(response.status_code, 200)

    def test_signup_func(self):
        found = resolve('/user/signup/')
        self.assertEqual(found.func, story9_views.signup)
    
    def test_signup_template(self):
        response = Client().get('/user/signup/')
        self.assertContains(response, 'Sign Up', count=2, html=True)
        self.assertTemplateUsed(response, template_name='signup.html')

    def test_signup_post(self):
        data_post = {'username':'test2', 'first_name':'testing', 'last_name':'testing2', 'email':'test@gmail.com','password1':'test@12345','password2':'test@12345'}
        request = Client().post('/user/signup/', data_post, follow=True)
        self.assertEqual(User.objects.count(), 1)