from django.urls import path, include
from . import views as story9_views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('signup/', story9_views.signup, name='signup'),
    path('login/', auth_views.LoginView.as_view(),{'template_name': 'login.html', 'next_page': 'profile'}, name='login'),
    path('logout/', auth_views.LogoutView.as_view(),{'next_page': 'login'} ,name='logout'),
    path('', story9_views.account, name='account')
]