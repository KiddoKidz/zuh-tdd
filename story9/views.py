from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from story9.forms import SignUpForm

# Create your views here.
def signup(request):
    form = SignUpForm()
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('story9:account')
    return render(request, 'signup.html', {'form': form})
    # return HttpResponse('Signup page')

@login_required
def account(request):
    return render(request, 'account.html')
    

