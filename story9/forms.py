from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import TextInput

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'id':'first_name', 'tag':'First Name'}))
    last_name = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'id':'last_name', 'tag':'Last Name'}))
    email = forms.EmailField(max_length=254, widget=forms.TextInput(attrs={'id':'email', 'tag':'Email','type':'email', 'class':'validate'}))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',)
        widgets = {
            'username': TextInput(attrs={'id':'username', 'tag':'Username'}),
            'password1': TextInput(attrs={'class':'validate','id':'password1', 'tag':'Password', 'type':'password'}),
            'password2': TextInput(attrs={'class':'validate','id':'password2', 'tag':'Password Confirmation', 'type':'password'}),
        }