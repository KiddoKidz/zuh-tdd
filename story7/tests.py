from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.apps import apps
import time
from .apps import Story7Config
from . import views 
from django.urls import resolve
import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
# Create your tests here.

class Story7UnitTest(TestCase):
    def test_app_name(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')

    def test_profile_url(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    
    def test_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, views.home)

    def test_profile_template(self):
        response = Client().get('/profile/')
        self.assertContains(response, 'Activities', count=1, html=True)

    
class Story7FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('headless')
        self.options.add_argument('no-sandbox')
        self.options.add_argument('disable-dev-shm-usage')
        self.options.add_argument('disable-extensions')
        self.browser = webdriver.Chrome(options=self.options, executable_path='./chromedriver')
        return super().setUp()
    
    def tearDown(self):
        self.browser.quit()
        return super().tearDown()

    def test_bgcolor_change(self):
        self.browser.get(self.live_server_url+'/profile/')
        time.sleep(10)
        temp = self.browser.find_element_by_tag_name('body')
        crnt_clr = temp.value_of_css_property('background-color')
        temp = self.browser.find_element_by_class_name("lever")
        
        actions = webdriver.ActionChains(self.browser)
        actions.click(temp)
        actions.perform()

        time.sleep(10)
        temp = self.browser.find_element_by_tag_name('body')
        temp = self.browser.find_element_by_tag_name("body")
        crnt_clr2 = temp.value_of_css_property('background-color')
        self.assertNotEqual(crnt_clr,crnt_clr2)